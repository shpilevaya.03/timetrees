﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees.ConsoleUI
{
    public struct Color
    {
        public const ConsoleColor HighlightColor =      ConsoleColor.DarkBlue;
        public const ConsoleColor BaseColor =           ConsoleColor.Black;

        public const ConsoleColor TextColor =           ConsoleColor.White;
        public const ConsoleColor TextHighlightColor =  ConsoleColor.Gray;
    }

    static class ConsoleHelper
    {
        public static void ClearLines(int line, int posX, int posY, int length)
        {
            while (length != 0)
            {
                Console.MoveBufferArea(0, line, Console.BufferWidth, 1, Console.BufferWidth, line, ' ', Console.ForegroundColor, Console.BackgroundColor);
                length--;
            }

            Console.SetCursorPosition(posX, posY);
        }

        public static void ClearScreen()
        {
            for (int i = 0; i < Console.WindowHeight; i++)
            {
                Console.Write(new string(' ', Console.WindowWidth), Console.BackgroundColor = Color.BaseColor);
                Console.SetCursorPosition(0, i);
            }
            Console.SetCursorPosition(0, 0);
        }

        public static bool GetYesOrNoAnswer()
        {
            Console.WriteLine("Нажмите Y, если ответ да и N - если нет");

            ConsoleKeyInfo key = Console.ReadKey();
            Console.WriteLine();
            return key.Key == ConsoleKey.Y;
        }

        public static void PrintHighlightedText(string context)
        {
            Console.WriteLine(context, Console.BackgroundColor = Color.HighlightColor);
            Console.BackgroundColor = Color.BaseColor;
        }
    }
}
