﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    class MainMenu : BaseMenu
    {
        protected override List<MenuItem> Items => new List<MenuItem>
        {
            new MenuItem() { Text = "Вычислить максимальную разницу дат между событиями", Logic = new DeltaLogic(), IsSelected = true },
            new MenuItem() { Text = "Найти количество людей младше 20 лет, родившихся в високосный год", Logic = new LeapYearLogic() },
            new MenuItem() { Text = "Вывести данные", Logic = new PrintDataLogic() },
            new MenuItem() { Text = "Пример поиска людей", Logic = new ChangePersonLogic() },
            new MenuItem() { Text = "Добавить данные", Logic = new AddDataLogic() },
            new MenuItem() { Text = "Выход"}
        };
    }
}
