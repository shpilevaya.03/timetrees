﻿using System;
using TimeTrees.Core;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    public class ChangePersonLogic : ILogic
    {
        const string _changeDataText = "ИЗМЕНЕНИЕ ДАННЫХ О ЧЕЛОВЕКЕ";
        static readonly string[] _changeData = new[] { "Имя", "Дата рождения", "Дата смерти" };

        const string nameProgramID =        "Имя";
        const string birthDateProgramID =   "Дата рождения";
        const string deathDateProgramID =   "Дата смерти";

        public void Execute()
        {
            PersonRepo personRepo = new PersonRepo();
            var people = personRepo.GetAllPeople();

            var findPersonMenu = new FindPersonMenu(_changeDataText);
            var person = findPersonMenu.FindPerson(people); 

            Console.WriteLine($"Вы выбрали человека: {person.Name}\nВы уверены, что хотите изменить данные о нём?");
            if (ConsoleHelper.GetYesOrNoAnswer())
            {
                ConsoleHelper.ClearScreen();
                ChangePerson(person);
            }
        }

        static void ChangePerson(Person foundPerson)
        {
            ConsoleHelper.PrintHighlightedText(_changeDataText);
            PrintPersonInfo(foundPerson);

            Console.WriteLine("Нажмите ENTER, если хотите оставить эти данные\n");

            foreach (var nameData in _changeData)
            {
                Console.WriteLine(nameData);
                string data = Console.ReadLine();
                if (!string.IsNullOrEmpty(data))
                    ExecuteChangeData(foundPerson, nameData, data);
                Console.WriteLine();
            }

            if (foundPerson.RelativesIDs.Count == 2)
            {
                Console.WriteLine("Хотите изменить родителей?");
                if (ConsoleHelper.GetYesOrNoAnswer())
                {
                    ChangeParentsMenu(foundPerson);
                }
            }
            else if (foundPerson.RelativesIDs.Count == 1)
            {
                Console.WriteLine("Хотите изменить или добавить родителей?");
                if(ConsoleHelper.GetYesOrNoAnswer())
                {
                    Console.WriteLine("\nХотите добавить родителей?");
                    if (ConsoleHelper.GetYesOrNoAnswer())
                    {
                        AddParentsMenu(foundPerson);
                    }

                    Console.WriteLine("\nХотите изменить родителей?");
                    if (ConsoleHelper.GetYesOrNoAnswer())
                    {
                        ChangeParentsMenu(foundPerson);
                    }
                }
            }
            else
            {
                Console.WriteLine("Хотите добавить родителей?");
                if (ConsoleHelper.GetYesOrNoAnswer())
                {
                    AddParentsMenu(foundPerson);
                }
            }
        }

        static void ChangeParentsMenu(Person person)
        {
            bool flag = true;
            var personRepo = new PersonRepo();

            while (flag)
            {
                var findPerson = new FindPersonMenu("Выберите родителя, которого вы хотите поменять");

                var relatives = person.RelativesIDs;
                Person removeParent = findPerson.FindPerson(relatives);

                person.RemoveRelative(removeParent);
                Console.WriteLine("Выберите родителя");
                Person addParent = findPerson.FindPerson(personRepo.GetAllPeople());
                person.AddRelative(addParent);

                ConsoleHelper.PrintHighlightedText("Хотите повторить?");
                flag = ConsoleHelper.GetYesOrNoAnswer();
            }
        }

        static void AddParentsMenu(Person person)
        {
            bool flag = true;
            var personRepo = new PersonRepo();
            var people = personRepo.GetAllPeople();

            while (flag && person.RelativesIDs.Count != 2)
            {
                var findPerson = new FindPersonMenu("Выберите родителя");

                var foundParent = findPerson.FindPerson(people);
                person.AddRelative(foundParent);

                if (person.RelativesIDs.Count != 2)
                {
                    ConsoleHelper.PrintHighlightedText("Хотите повторить?");
                    flag = ConsoleHelper.GetYesOrNoAnswer();
                }
            }
        }

        static void ExecuteChangeData(Person person, string tag, string contents)
        {
            switch (tag)
            {
                case nameProgramID:
                    person.ChangeName(contents);
                    break;

                case birthDateProgramID:
                    var birthDate = DateTimeExtensions.ToDateTime(contents);
                    while (birthDate.Year >= 2022)
                    {
                        Console.WriteLine("Некорректный ввод даты рождения! Повторите попытку");
                        birthDate = DateTimeExtensions.ToDateTime(Console.ReadLine());
                    }
                    person.ChangeBirthDate(birthDate);
                    break;

                case deathDateProgramID:
                    var deathDate = DateTimeExtensions.ToDateTime(contents);
                    while (deathDate < person.BirthDate || deathDate.Year - person.BirthDate.Year > 130)
                    {
                        Console.WriteLine("Некорректный ввод даты смерти! Повторите попытку");
                        deathDate = DateTimeExtensions.ToDateTime(Console.ReadLine());
                    }
                    person.ChangeDeathDate(deathDate);
                    break;
            }
        }

        static void PrintPersonInfo(Person person)
        {
            var IDs = "";
            person.RelativesIDs.ForEach(p => IDs += p.ID + " ");

            Console.WriteLine(
                $"Имя: {person.Name}\n" +
                $"Дата рождения: {person.BirthDate.ToShortDateString()}\n" +
                $"Дата смерти: " + (person.DeathDate != null ? ((DateTime)person.DeathDate).ToShortDateString() : "отсутствует") + "\n" +
                $"ID родителей: {IDs}\n"
                );
        }
    }
}
