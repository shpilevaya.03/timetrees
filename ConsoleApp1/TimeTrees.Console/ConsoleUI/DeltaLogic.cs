﻿using System;
using TimeTrees.Core;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    public class DeltaLogic : ILogic
    {
        public void Execute()
        {
            var timelineEventRepo = new TimelineEventRepo();
            var timelines = timelineEventRepo.GetAllTimelineEvents();

            var (years, months, days) = Timeline.MaximumDifference(timelines);
            Console.WriteLine($"Между событиями прошло {years} лет, {months} месяцев и {days} дней");
        }
    }
}
