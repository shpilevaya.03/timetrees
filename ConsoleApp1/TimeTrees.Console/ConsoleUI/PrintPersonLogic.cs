﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeTrees.Core;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    public class PrintPersonLogic : ILogic
    {
        public void Execute()
        {
            var personRepo = new PersonRepo();
            var people = personRepo.GetAllPeople();

            (int maxLengthName, int maxLengthID) = (0, 0);
            for (int i = 0; i < people.Count; i++)
            {
                maxLengthName = Math.Max(maxLengthName, people[i].Name.Length);
                maxLengthID = Math.Max(maxLengthID, people[i].ID.ToString().Length);
            }

            Console.WriteLine($"ID{new string(' ', maxLengthID)}\t Имя{new string(' ', maxLengthName > 3 ? maxLengthName - 3 : maxLengthName)}\t " +
                $"Дата рождения\t Дата смерти\t ID родителей\n",
                Console.ForegroundColor = Color.TextHighlightColor);
            Console.ForegroundColor = Color.TextColor;

            foreach (var person in people)
            {
                Console.Write($"{person.ID}{new string(' ', maxLengthID - person.ID.ToString().Length)}\t {person.Name}{new string(' ', maxLengthName - person.Name.Length)}\t " +
                    $"{person.BirthDate.ToShortDateString()}\t " +
                    $"{((person.DeathDate != null) ? ((DateTime)person.DeathDate).ToShortDateString() : "Отсутствует")}\t ");
                if (person.RelativesIDs != null)
                    person.RelativesIDs.ForEach(human => Console.Write(human.ID + " "));
                Console.WriteLine();
            }
        }
    }
}
