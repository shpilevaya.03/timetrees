﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeTrees.Core;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    public class PrintTimelineLogic : ILogic
    {
        public void Execute()
        {
            var timelineEventRepo = new TimelineEventRepo();
            var timelines = timelineEventRepo.GetAllTimelineEvents();

            int maxLength = 0;
            for (int i = 0; i < timelines.Count; i++)
            {
                maxLength = Math.Max(maxLength, timelines[i].EventDescription.Length);
            }

            Console.WriteLine($"Дата события\t Описание события{new string(' ', maxLength - 16)}\t ID людей, связанных с этим событием\n",
                Console.ForegroundColor = Color.TextHighlightColor);
            Console.ForegroundColor = Color.TextColor;

            foreach (var timeline in timelines)
            {
                Console.Write($"{timeline.EventDate.ToShortDateString()}\t {timeline.EventDescription + new string(' ', maxLength - timeline.EventDescription.Length)}\t ");
                timeline.EventPeople.ForEach(id => Console.Write(id.ID + " "));
                Console.WriteLine();
            }
        }
    }
}
