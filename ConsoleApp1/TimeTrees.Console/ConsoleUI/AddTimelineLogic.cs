﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeTrees.Core;
using System.Linq;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    public class AddTimelineLogic : ILogic
    {
        public void Execute()
        {
            PersonRepo personRepo = new PersonRepo();
            TimelineEventRepo timelineEventRepo = new TimelineEventRepo();

            ConsoleHelper.PrintHighlightedText("ДОБАВЛЕНИЕ СОБЫТИЯ");
            Console.WriteLine("Введите дату события (yyyy-MM-dd)");
            DateTime date = DateTimeExtensions.ToDateTime(Console.ReadLine());
            while (date == new DateTime())
            {
                Console.WriteLine("Некорректный ввод даты события! Повторите попытку");
                date = DateTimeExtensions.ToDateTime(Console.ReadLine());
            }
            Console.WriteLine("Введите описание события");
            var description = Console.ReadLine();
            Console.WriteLine("Если есть люди, связанные с этим событием, нажмите F");
            var key = Console.ReadKey().Key;

            List<Person> eventPeople = new List<Person>();
            if (key == ConsoleKey.F)
            {
                bool flag = true;
                while(flag)
                {
                    var findPerson = new FindPersonMenu("Выберите человека из списка");
                    var foundPerson = findPerson.FindPerson(personRepo.GetAllPeople());

                    Console.WriteLine("Вы уверены, что хотите добавить человека?");
                    if(ConsoleHelper.GetYesOrNoAnswer())
                        eventPeople.Add(foundPerson);

                    Console.WriteLine("\nХотите продолжить?");
                    flag = ConsoleHelper.GetYesOrNoAnswer();
                }
            }

            Console.WriteLine("\nВы уверены, что хотите добавить это событие?", Console.BackgroundColor = Color.HighlightColor);
            Console.BackgroundColor = Color.BaseColor;

            if (ConsoleHelper.GetYesOrNoAnswer())
            {
                timelineEventRepo.AddTimelineEvent(new TimelineEvent(date, description, eventPeople));
            }
        }
    }
}
