﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    class DataLogicMenu : BaseMenu
    {
        protected override List<MenuItem> Items => new List<MenuItem>
            {
                new MenuItem() { Logic = new AddTimelineLogic(), Text = "Добавить событие", IsSelected = true },
                new MenuItem() { Logic = new AddPersonLogic(), Text = "Добавить человека" },
                new MenuItem() { Text = "Назад"}
            };
    }

    class AddDataLogic : ILogic
    {
        public void Execute()
        {
            DataLogicMenu menu = new DataLogicMenu();
            menu.SubMenu();
        }
    }
}
