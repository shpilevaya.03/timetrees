﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    class PrintDataLogicMenu : BaseMenu
    {
        protected override List<MenuItem> Items => new List<MenuItem>
            {
                new MenuItem() { Logic = new PrintTimelineLogic(), Text = "Вывести на экран события", IsSelected = true },
                new MenuItem() { Logic = new PrintPersonLogic(), Text = "Вывести на экран людей" },
                new MenuItem() { Text = "Назад"}
            };
    }

    class PrintDataLogic : ILogic
    {
        public void Execute()
        {
            PrintDataLogicMenu menu = new PrintDataLogicMenu();
            menu.SubMenu();
        }
    }
}
