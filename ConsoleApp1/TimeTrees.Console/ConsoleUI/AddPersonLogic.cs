﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeTrees.Core;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    public class AddPersonLogic : ILogic
    {
        public void Execute()
        {
            PersonRepo personRepo = new PersonRepo();
            List<Person> people = personRepo.GetAllPeople();

            ConsoleHelper.PrintHighlightedText("ДОБАВЛЕНИЕ ЧЕЛОВЕКА");
            Console.WriteLine("Введите имя человека");
            var name = Console.ReadLine();

            var ID = ++PeopleIDGenerator.MaxID;

            Console.WriteLine("Введите дату рождения (yyyy-MM-dd)");
            var birthDate = DateTimeExtensions.ToDateTime(Console.ReadLine());
            while (birthDate.Year >= 2022 || birthDate == new DateTime())
            {
                Console.WriteLine("Некорректный ввод даты рождения! Повторите попытку");
                birthDate = DateTimeExtensions.ToDateTime(Console.ReadLine());
            }

            Console.WriteLine("Введите дату смерти (при наличии) (yyyy-MM-dd)");
            var deathDate = DateTimeExtensions.ToDateTime(Console.ReadLine());
            while (deathDate != new DateTime() && (deathDate < birthDate || deathDate.Year - birthDate.Year > 130))
            {
                Console.WriteLine("Некорректный ввод даты смерти! Повторите попытку");
                deathDate = DateTimeExtensions.ToDateTime(Console.ReadLine());
            }

            Console.WriteLine("Нажмите F, чтобы найти родственников, либо нажмите Enter, если они отсутствуют/неизвестны");

            List<Person> parents = new List<Person>();
            var array = new[] { "отец", "мать" };
            foreach (var relative in array)
            {
                Console.WriteLine($"Нажмите ENTER, если у человека неизвестен(на) {relative}, и F, если нужно добавить.");

                ConsoleKeyInfo keyInfo;
                do
                {
                    keyInfo = Console.ReadKey();
                    if (keyInfo.Key != ConsoleKey.Enter && keyInfo.Key != ConsoleKey.F)
                    {
                        Console.WriteLine("Неизвестная клавиша. Введите ENTER или F.");
                    }

                } while (keyInfo.Key != ConsoleKey.Enter && keyInfo.Key != ConsoleKey.F);

                if (keyInfo.Key == ConsoleKey.F)
                {
                    ConsoleHelper.ClearScreen();
                    var findPersonMenu = new FindPersonMenu();
                    Person found = findPersonMenu.FindPerson(people);
                    parents.Add(found);
                }
            }

            ConsoleHelper.PrintHighlightedText("Вы уверены, что хотите добавить этого человека?");

            if (ConsoleHelper.GetYesOrNoAnswer())
            {
                personRepo.AddPerson(new Person(ID, name, birthDate, deathDate == new DateTime() ? (DateTime?)null : deathDate, parents));
            }
        }
    }
}
