﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeTrees.Core;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    public class LeapYearLogic : ILogic
    {
        public void Execute()
        {
            var personRepo = new PersonRepo();
            var people = personRepo.GetAllPeople();

            Console.WriteLine("Следующие люди родились в високосный год и их возраст менее 20 лет");
            List<Person> found = PeopleExtensions.FindPeopleBornInLeapYear(people);
            foreach (var person in found)
                Console.WriteLine($"{person.Name}, родился(ась) в {person.BirthDate.Year} году, ему(ей) {person.FindAge()} лет");
        }
    }
}
