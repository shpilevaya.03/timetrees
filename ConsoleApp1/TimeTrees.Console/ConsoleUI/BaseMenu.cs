﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimeTrees.Core;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    public abstract class BaseMenu
    {
        protected class MenuItem
        {
            public string Text;
            public bool IsSelected;
            public ILogic Logic;
        }

        protected abstract List<MenuItem> Items { get; }
        List<MenuItem> _items = new List<MenuItem>();

        public BaseMenu()
        {
            _items = Items.ToList();
        }

        public void SubMenu()
        {
            bool back = false;

            do
            {
                Draw();

                ConsoleKeyInfo key = Console.ReadKey(true);

                switch (key.Key)
                {
                    case ConsoleKey.DownArrow:
                        Next();
                        break;
                    case ConsoleKey.UpArrow:
                        Prev();
                        break;
                    case ConsoleKey.Enter:
                        var selectedItem = _items.First(x => x.IsSelected);

                        back = selectedItem.Logic == null;

                        if (!back)
                        {
                            ConsoleHelper.ClearScreen();
                            selectedItem.Logic.Execute();
                            Console.WriteLine();
                            ConsoleHelper.PrintHighlightedText("Для выхода нажмите любую клавишу");
                            Console.ReadKey();
                        }
                        break;
                }
            }
            while (!back);
        }

        public virtual void Draw()
        {
            ConsoleHelper.ClearScreen();

            foreach (var menuItem in _items)
            {
                Console.BackgroundColor = menuItem.IsSelected 
                    ? Color.HighlightColor 
                    : Color.BaseColor;

                Console.WriteLine("| " + menuItem.Text);
            }

            Console.BackgroundColor = Color.BaseColor;
        }

        public void Next()
        {
            var selectedItem = _items.First(x => x.IsSelected);
            var selectedInd = _items.IndexOf(selectedItem);
            selectedItem.IsSelected = false;

            selectedInd = selectedInd == _items.Count - 1 
                ? 0 
                : ++selectedInd;

            _items[selectedInd].IsSelected = true;
        }

        public void Prev()
        {
            var selectedItem = _items.First(x => x.IsSelected);
            var selectedInd = _items.IndexOf(selectedItem);
            selectedItem.IsSelected = false;

            selectedInd = selectedInd <= 0 
                ? _items.Count - 1 
                : --selectedInd;

            _items[selectedInd].IsSelected = true;
        }

        public bool Select()
        {
            var selectedItem = _items.First(x => x.IsSelected);

            if (selectedItem.Logic != null)
            {
                ConsoleHelper.ClearScreen();
                selectedItem.Logic.Execute();
                if (!(selectedItem.Logic is AddDataLogic) && !(selectedItem.Logic is PrintDataLogic))
                {
                    Console.WriteLine();
                    ConsoleHelper.PrintHighlightedText("Для выхода нажмите любую клавишу");
                    Console.ReadKey();
                }
            }
            else
            {
                var fileWriter = new FileWriter();
                var personRepo = new PersonRepo();
                var timelineEventRepo = new TimelineEventRepo();

                fileWriter.SavePeople(personRepo.GetAllPeople());
                fileWriter.SaveTimelineEvents(timelineEventRepo.GetAllTimelineEvents());
            }

            return selectedItem.Logic == null;
        }
    }
}
