﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees.ConsoleUI
{
    public interface ILogic
    {
        void Execute();
    }
}
