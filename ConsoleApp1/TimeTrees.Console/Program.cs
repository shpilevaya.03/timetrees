﻿using TimeTrees.Core;
using TimeTrees.ConsoleUI.ConsoleUI;
using System;

namespace TimeTrees.ConsoleUI
{
    class Program
    {
        public static PersonRepo personRepo;
        public static TimelineEventRepo timelineEventRepo;
        public static FileWriter fileWriter;

        static void Main()
        {
            personRepo = new PersonRepo();
            timelineEventRepo = new TimelineEventRepo();
            fileWriter = new FileWriter();

            personRepo.Init();
            timelineEventRepo.Init();

            Console.CursorVisible = false;
            MainMenu menu = new MainMenu();
            bool exit = false;

            do
            {
                menu.Draw();

                ConsoleKeyInfo key = Console.ReadKey(true);
                switch (key.Key)
                {
                    case ConsoleKey.DownArrow:
                        menu.Next();
                        break;
                    case ConsoleKey.UpArrow:
                        menu.Prev();
                        break;
                    case ConsoleKey.Enter:
                        exit = menu.Select();
                        break;
                }
            }
            while (!exit);
        }
    }
}
