﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TimeTrees.Core;

namespace TimeTrees.Desktop
{
    internal class PersonArgs
    {
        public TextBox Name             { get; private set; }
        public TextBox BirthDate        { get; private set; }
        public TextBox DeathDate        { get; private set; }
        public Button btnSaveData       { get; private set; }

        public PersonArgs(Button SaveData, TextBox Name, TextBox BirthDate, TextBox DeathDate)
        {
            this.btnSaveData =  SaveData;
            this.Name =         Name;
            this.BirthDate =    BirthDate;
            this.DeathDate =    DeathDate;
        }

        public void RemoveText()
        {
            Name.Text =         null;
            BirthDate.Text =    null;
            DeathDate.Text =    null;
        }

        public void PrintPersonData(Person person)
        {
            Name.Text = person.Name;
            BirthDate.Text = DateTimeExtensions.ToShortDateString(person.BirthDate);
            DeathDate.Text = person.DeathDate is not null
                ? DateTimeExtensions.ToShortDateString((DateTime)person.DeathDate)
                : null;
        }
    }
}