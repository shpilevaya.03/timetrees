﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using TimeTrees.Core;

namespace TimeTrees.Desktop.Tools
{
    internal class ArrowTool : Tool
    {
        ToolArgs toolArgs;
        PersonArgs personArgs;
        SettingsShapes settings;
        Grid grid;

        public ArrowTool(ToolArgs toolArgs, PersonArgs personArgs) : base(toolArgs)
        {
            toolArgs.statusBarUpdater.UpdateCurrentState(StateBar.None);

            this.toolArgs = toolArgs;
            this.personArgs = personArgs;

            settings = new SettingsShapes();

            AddRectangleEvents();
        }

        private void ShapeDragOnMouseMove(object sender, MouseEventArgs e)
        {
            var grid = sender as Grid;
            var rect = grid.Children.OfType<Rectangle>().FirstOrDefault();
            if (!grid.IsMouseCaptured) return;
            Canvas.SetLeft(grid, Mouse.GetPosition(toolArgs.canvas).X - rect.Width/2);
            Canvas.SetTop(grid, Mouse.GetPosition(toolArgs.canvas).Y - rect.Height/2);

            foreach (var shapeInfo in toolArgs.personShapeRepo.GetConnectionInfos())
            {
                if (shapeInfo.Person.GridShape == grid)
                {
                    shapeInfo.Connection.Points[0] = 
                        settings.GetBasePoint((Mouse.GetPosition(toolArgs.canvas).X - rect.Width / 2,
                        Mouse.GetPosition(toolArgs.canvas).Y - rect.Height / 2),
                        (rect.Width, rect.Height), shapeInfo.RelativeType, false);
                    Updates.UpdatePoints(shapeInfo.Connection.Points, shapeInfo.RelativeType);
                }
                else if (shapeInfo.RelatedPerson.GridShape == grid)
                {
                    shapeInfo.Connection.Points[shapeInfo.Connection.Points.Count-1] = 
                        settings.GetBasePoint((Mouse.GetPosition(toolArgs.canvas).X - rect.Width / 2,
                        Mouse.GetPosition(toolArgs.canvas).Y - rect.Height / 2),
                        (rect.Width, rect.Height), shapeInfo.RelativeType, true);
                    Updates.UpdatePoints(shapeInfo.Connection.Points, shapeInfo.RelativeType);
                }
            }
        }

        private void ShapeDragOnMouseDown(object sender, MouseButtonEventArgs e)
        {
            var grid = sender as Grid;
            this.grid = grid;
            var rect = grid.Children.OfType<Rectangle>().FirstOrDefault();

            var shape = toolArgs.personShapeRepo.FindPersonShape(grid);
            personArgs.PrintPersonData(shape.Info);

            if (!personArgs.btnSaveData.IsEnabled)
            {
                personArgs.btnSaveData.IsEnabled = true;
                personArgs.btnSaveData.Click += ChangeData_Click;
            }

            grid.CaptureMouse();
        }

        private void ChangeData_Click(object sender, RoutedEventArgs e)
        {
            var shape = toolArgs.personShapeRepo.FindPersonShape(grid);
            shape.Info.ChangeData(
                personArgs.Name.Text,
                DateTimeExtensions.ToDateTime(personArgs.BirthDate.Text),
                DateTimeExtensions.ToDateTime(personArgs.DeathDate.Text)
                );

            Updates.UpdateTextBoxes(grid, shape.Info);

            personArgs.RemoveText();
            personArgs.btnSaveData.IsEnabled = false;
            personArgs.btnSaveData.Click -= ChangeData_Click;
        }

        private void ShapeDragOnMouseUp(object sender, MouseButtonEventArgs e)
        {
            var grid = sender as Grid;
            grid.ReleaseMouseCapture();
        }

        private void AddRectangleEvents()
        {
            var canvas = toolArgs.canvas;

            foreach (var component in canvas.Children)
            {
                if (component is Grid grid)
                {
                    grid.MouseLeftButtonDown += ShapeDragOnMouseDown;
                    grid.MouseLeftButtonUp +=   ShapeDragOnMouseUp;
                    grid.MouseMove +=           ShapeDragOnMouseMove;
                }
            }
        }

        private void RemoveRectangleEvents()
        {
            var canvas = toolArgs.canvas;

            foreach (var component in canvas.Children)
            {
                if (component is Grid grid)
                {
                    grid.MouseLeftButtonDown -= ShapeDragOnMouseDown;
                    grid.MouseLeftButtonUp -=   ShapeDragOnMouseUp;
                    grid.MouseMove -=           ShapeDragOnMouseMove;
                }
            }
        }

        public override void Unload()
        {
            personArgs.btnSaveData.Click -= ChangeData_Click;
            personArgs.btnSaveData.IsEnabled = false;
            personArgs.RemoveText();
            RemoveRectangleEvents();
            Dispose();
        }
    }
}
