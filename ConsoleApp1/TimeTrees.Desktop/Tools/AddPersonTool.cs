﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using TimeTrees.Core;

namespace TimeTrees.Desktop.Tools
{
    internal class AddPersonTool : Tool
    {
        ToolArgs toolArgs;
        PersonArgs personArgs;
        Grid newGrid;
        PersonShape shape;
        Person personInfo;
        bool gridIsSaved;

        public AddPersonTool(ToolArgs toolArgs, PersonArgs personArgs) : base(toolArgs)
        {
            toolArgs.statusBarUpdater.UpdateCurrentState(StateBar.AddPerson);

            shape = new PersonShape();
            gridIsSaved = false;

            this.toolArgs = toolArgs;
            this.personArgs = personArgs;
            newGrid = SetGrid();
            newGrid.CaptureMouse();

            newGrid.MouseMove           += OnMouseMove;
            newGrid.MouseLeftButtonDown += OnMouseLeftButtonDown;
        }

        private void OnMouseLeftButtonDown(object sender, MouseEventArgs e)
        {
            Grid grid = sender as Grid;

            if (!gridIsSaved)
            {
                toolArgs.personShapeRepo.AddPersonShape(shape);
                personArgs.btnSaveData.IsEnabled = true;
                personArgs.btnSaveData.Click += SaveData_Click;
            }

            gridIsSaved = true;
            grid.ReleaseMouseCapture();
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            Grid grid = sender as Grid;
            if (!grid.IsMouseCaptured) return;
            Canvas.SetLeft(grid, Mouse.GetPosition(toolArgs.canvas).X);
            Canvas.SetTop(grid, Mouse.GetPosition(toolArgs.canvas).Y);
        }

        private Grid SetGrid()
        {
            var settings = new SettingsShapes();
            var newGrid = settings.MakeGrid();

            Canvas.SetLeft(newGrid, Mouse.GetPosition(toolArgs.canvas).X);
            Canvas.SetTop(newGrid, Mouse.GetPosition(toolArgs.canvas).Y);

            toolArgs.canvas.Children.Add(newGrid);
            shape.GridShape = newGrid;

            return newGrid;
        }

        private void SaveData_Click(object sender, RoutedEventArgs e)
        {
            personInfo = new Person(
                personArgs.Name.Text,
                DateTimeExtensions.ToDateTime(personArgs.BirthDate.Text),
                DateTimeExtensions.ToDateTime(personArgs.DeathDate.Text),
                new List<(int, RelativeType)>()
                );

            shape.Info = personInfo;

            Updates.UpdateTextBoxes(newGrid, personInfo);

            personArgs.RemoveText();
            personArgs.btnSaveData.IsEnabled = false;
        }

        public override void Unload()
        {
            if(shape.Info is null)
                shape.Info = new Person("", new DateTime(), null, new List<(int, RelativeType)>());

            newGrid.MouseMove               -= OnMouseMove;
            newGrid.MouseLeftButtonDown     -= OnMouseLeftButtonDown;
            personArgs.btnSaveData.Click    -= SaveData_Click;
            personArgs.btnSaveData.IsEnabled = false;
            Dispose();
        }
    }
}
