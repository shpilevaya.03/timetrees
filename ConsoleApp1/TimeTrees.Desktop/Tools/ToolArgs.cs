﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TimeTrees.Core;

namespace TimeTrees.Desktop.Tools
{
    internal class ToolArgs
    {
        public MainWindow mainWindow                { get; private set; }
        public Canvas canvas                        { get; private set; }
        public StatusBarUpdater statusBarUpdater    { get; private set; }
        public ShapeRepo personShapeRepo            { get; private set; }
        public RelativeType correctRelative         { get; set; }

        public ToolArgs(MainWindow mainWindow, Canvas canvas, StatusBarUpdater statusBarUpdater, ShapeRepo personShapeRepo)
        {
            this.mainWindow =       mainWindow;
            this.canvas =           canvas;
            this.statusBarUpdater = statusBarUpdater;
            this.personShapeRepo =  personShapeRepo;
            correctRelative =       RelativeType.None;
        }
    }
}
