﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;

namespace TimeTrees.Desktop.Tools
{
    internal class RemoveShapeTool : Tool
    {
        ToolArgs args;

        public RemoveShapeTool(ToolArgs args) : base(args)
        {
            args.statusBarUpdater.UpdateCurrentState(StateBar.RemovePerson);

            this.args = args;

            AddEvent();
        }

        private void AddEvent()
        {
            var canvas = args.canvas;

            foreach (var component in canvas.Children)
            {
                if (component is Grid grid)
                {
                    grid.MouseLeftButtonDown += OnMouseLeftButtonDown;
                }
            }
        }

        private void RemoveEvent()
        {
            var canvas = args.canvas;

            foreach (var component in canvas.Children)
            {
                if (component is Grid grid)
                {
                    grid.MouseLeftButtonDown -= OnMouseLeftButtonDown;
                }
            }
        }

        private void OnMouseLeftButtonDown(object sender, MouseEventArgs e)
        {
            var grid = sender as Grid;

            foreach (var shapeInfo in args.personShapeRepo.GetConnectionInfos())
            {
                if(shapeInfo.Person.GridShape == grid || shapeInfo.RelatedPerson.GridShape == grid)
                {
                    if (shapeInfo.Person.GridShape == grid)
                    {
                        shapeInfo.RelatedPerson.Info.RemoveRelative(shapeInfo.Person.Info, args.personShapeRepo.FindConnectionInfo(shapeInfo.Person, shapeInfo.RelatedPerson).RelativeType);
                    }
                    else
                    {
                        shapeInfo.Person.Info.RemoveRelative(shapeInfo.RelatedPerson.Info, args.personShapeRepo.FindConnectionInfo(shapeInfo.RelatedPerson, shapeInfo.Person).RelativeType);
                    }
                    args.canvas.Children.Remove(shapeInfo.Connection);
                }
            }
            args.personShapeRepo.RemoveShape(grid);

            args.canvas.Children.Remove(grid);
        }

        public override void Unload()
        {
            RemoveEvent();
            Dispose();
        }
    }
}
