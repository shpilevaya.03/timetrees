﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using TimeTrees.Core;

namespace TimeTrees.Desktop.Tools
{
    internal class ConnectionTool : Tool
    {
        ToolArgs args;
        Polyline polyline;
        PointCollection points;
        SettingsShapes settings;
        RelativeType relativeType;

        ConnectionInfo shapeInfo;

        public ConnectionTool(ToolArgs args) : base(args)
        {
            args.statusBarUpdater.UpdateCurrentState(StateBar.Connection);

            shapeInfo = new ConnectionInfo();
            points =    new PointCollection();
            settings =  new SettingsShapes();

            this.args = args;
            AddGridEvent(SetLineOnShape);
        }

        private void SetLineOnShape(object sender, MouseEventArgs e)
        {
            relativeType = args.correctRelative;
            var grid = sender as Grid;
            var rect = grid.Children.OfType<Rectangle>().FirstOrDefault();
            shapeInfo.Person = args.personShapeRepo.FindPersonShape(grid);

            polyline = settings.DrawPolyline(relativeType);

            points.Add(settings.GetBasePoint((Canvas.GetLeft(grid), Canvas.GetTop(grid)), (rect.Width, rect.Height), relativeType, false));
            points.Add(points[0]);
            polyline.Points = points;

            shapeInfo.Connection = polyline;

            args.canvas.Children.Add(polyline);

            RemoveRectangleEvent(SetLineOnShape);
            AddGridEvent(SetPolylineOnShapeDown);

            args.canvas.MouseMove += CanvasOnMouseMove;
        }

        private void SetPolylineOnShapeDown(object sender, MouseButtonEventArgs e)
        {
            var grid = sender as Grid;
            var rect = grid.Children.OfType<Rectangle>().FirstOrDefault();
            shapeInfo.RelatedPerson = args.personShapeRepo.FindPersonShape(grid);

            args.canvas.MouseMove -= CanvasOnMouseMove;

            if (shapeInfo.Person != shapeInfo.RelatedPerson 
                && !ShapeInfoExtensions.ConnectionIsExists(args.personShapeRepo.GetConnectionInfos(), shapeInfo))
            {
                shapeInfo.Person.Info.AddRelative(shapeInfo.RelatedPerson.Info, relativeType);

                shapeInfo.RelatedPerson.Info.AddRelative(shapeInfo.Person.Info, relativeType == RelativeType.Parent ? RelativeType.Child : (relativeType == RelativeType.Child ? RelativeType.Parent : relativeType));
                points[points.Count - 1] = settings.GetBasePoint((Canvas.GetLeft(grid), Canvas.GetTop(grid)), (rect.Width, rect.Height), relativeType, true);

                Updates.UpdatePoints(points, relativeType);
                shapeInfo.RelativeType = relativeType;
                args.personShapeRepo.AddRectangleConnectionInfo(shapeInfo);
            }
            else
                args.canvas.Children.Remove(polyline);

            shapeInfo = new ConnectionInfo();
            polyline = null;
            points = new PointCollection();

            RemoveRectangleEvent(SetPolylineOnShapeDown);
            AddGridEvent(SetLineOnShape);
        }

        private void CanvasOnMouseMove(object sender, MouseEventArgs e)
        {
            var canvas = sender as Canvas;

            Updates.UpdatePoints(points, relativeType);
            points[points.Count-1] = new Point(Mouse.GetPosition(canvas).X, Mouse.GetPosition(canvas).Y);
        }

        private void AddGridEvent(MouseButtonEventHandler eventHandler)
        {
            var canvas = args.canvas;

            foreach (var component in canvas.Children)
            {
                if (component is Grid grid)
                {
                    grid.MouseLeftButtonDown += eventHandler;
                }
            }
        }

        private void RemoveRectangleEvent(MouseButtonEventHandler eventHandler)
        {
            var canvas = args.canvas;

            foreach (var component in canvas.Children)
            {
                if (component is Grid grid)
                {
                    grid.MouseLeftButtonDown -= eventHandler;
                }
            }
        }

        public override void Unload()
        {
            RemoveRectangleEvent(SetPolylineOnShapeDown);
            RemoveRectangleEvent(SetLineOnShape);
            args.canvas.MouseMove -= CanvasOnMouseMove;
            if (shapeInfo is not null)
                args.canvas.Children.Remove(shapeInfo.Connection);
            Dispose();
        }
    }
}
