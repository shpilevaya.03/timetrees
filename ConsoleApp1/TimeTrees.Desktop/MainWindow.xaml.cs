﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TimeTrees.Core;
using TimeTrees.Desktop.Tools;

namespace TimeTrees.Desktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Tool                tool = null;
        ToolArgs            toolArgs;
        PersonArgs          personArgs;
        StatusBarUpdater    statusUpdater;

        public MainWindow()
        {
            InitializeComponent();

            statusUpdater = new StatusBarUpdater(lblCoordinateInfo, lblStateInfo);
            toolArgs = new ToolArgs(this, mainCanvas, statusUpdater, new ShapeRepo());
            personArgs = new PersonArgs(btnSaveData, txtBoxPersonName, txtBoxPersonBirthDate, txtBoxPersonDeathDate);
            this.DataContext = toolArgs;

            tool = new ArrowTool(toolArgs, personArgs);
        }

        private void btnAddPerson_Click(object sender, RoutedEventArgs e)
        {
            if(tool != null) tool.Unload();
            tool = new AddPersonTool(toolArgs, personArgs);
        }

        private void btnConnection_Click(object sender, RoutedEventArgs e)
        {
            if (tool != null) tool.Unload();
            tool = new ConnectionTool(toolArgs);
        }

        private void btnRemovePerson_Click(object sender, RoutedEventArgs e)
        {
            if (tool != null) tool.Unload();
            tool = new RemoveShapeTool(toolArgs);
        }

        private void btnArrow_Click(object sender, RoutedEventArgs e)
        {
            if (tool != null) tool.Unload();
            tool = new ArrowTool(toolArgs, personArgs);
        }

        private void btnUploadCanvas_Click(object sender, RoutedEventArgs e)
        {
            toolArgs.personShapeRepo.RemoveData();
            var uploadingCanvas = new UploadingCanvas(toolArgs);
            uploadingCanvas.OpenCanvas();
            btnArrow_Click(sender, e);
        }

        private void btnSaveCanvas_Click(object sender, RoutedEventArgs e)
        {
            var saveCanvas = new SaverCanvas(toolArgs);
            saveCanvas.SaveCanvas();
        }

        private void mainCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            statusUpdater.UpdateCoordinatesLabel(Mouse.GetPosition(this));
        }

        private void sampleGrid_MouseMove(object sender, MouseEventArgs e)
        {
            lblConnectionType.Content = toolArgs.correctRelative.ToString();
        }
    }
}
