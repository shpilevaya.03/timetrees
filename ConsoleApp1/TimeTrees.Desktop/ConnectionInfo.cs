﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;
using TimeTrees.Core;

namespace TimeTrees.Desktop
{
    /// <summary>
    /// Информация о связи двух людей
    /// </summary>
    internal class ConnectionInfo
    {
        public PersonShape          Person;
        public RelativeType         RelativeType;
        public Polyline             Connection;
        public PersonShape          RelatedPerson;
    }

    /// <summary>
    /// Связь человека и прямоугольника
    /// </summary>
    class PersonShape
    {
        public Grid     GridShape;
        public Person   Info;
    }
}
