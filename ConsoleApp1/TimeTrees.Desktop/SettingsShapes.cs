﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using TimeTrees.Core;

namespace TimeTrees.Desktop
{
    internal class SettingsShapes
    {
        const int rectHeight = 120;
        const int rectWidth =  120;

        public Polyline DrawPolyline(RelativeType relativeType)
        {
            var stroke =            Brushes.Black;
            double thickness =      2;
            var strokeDashArray =   new DoubleCollection();

            switch (relativeType)
            {
                case RelativeType.Parent:
                case RelativeType.Child:            stroke = Brushes.BurlyWood; break;
                case RelativeType.SisterOrBrother:  stroke = Brushes.OrangeRed; break;
                case RelativeType.Spouses:          stroke = Brushes.Brown;     break;
                case RelativeType.FormerSpouses:    stroke = Brushes.Brown;
                    strokeDashArray = new DoubleCollection() { 9, 6 }; 
                    break;
                default: thickness = 0.5; break;
            }

            var polyline = new Polyline()
            {
                Stroke =            stroke,
                StrokeThickness =   thickness,
                StrokeDashArray =   strokeDashArray,
                StrokeEndLineCap =  PenLineCap.Round,
                StrokeLineJoin =    PenLineJoin.Round,
                StrokeDashCap =     PenLineCap.Round,
            };
            Panel.SetZIndex(polyline, -1);
            return polyline;
        }

        public Point GetBasePoint((double left, double top) coordinates, (double width, double height) rect, RelativeType relativeType, bool isRelative)
        {
            Point point = new Point();
            var type = !isRelative ? relativeType 
                : (relativeType == RelativeType.Parent 
                ? RelativeType.Child
                : (relativeType == RelativeType.Child 
                ? RelativeType.Parent : relativeType));

            switch(type)
            {
                case RelativeType.Parent:
                    point = new Point(coordinates.left + rect.width / 2, coordinates.top + rect.height);
                    break;
                case RelativeType.Child:
                    point = new Point(coordinates.left + rect.width / 2, coordinates.top);
                    break;
                default:
                    point = new Point(coordinates.left + rect.width / 2, coordinates.top + rect.height / 2);
                    break;
            }
            return point;
        }

        public PointCollection GetPoints(Grid grid, Grid relatedGrid, RelativeType type)
        {
            PointCollection points = new PointCollection();
            var relatedRect = relatedGrid.Children.OfType<Rectangle>().FirstOrDefault();
            var rect = grid.Children.OfType<Rectangle>().FirstOrDefault();
            points.Add(GetBasePoint((Canvas.GetLeft(grid), Canvas.GetTop(grid)), (rect.Width, rect.Height), type, false));
            points.Add(GetBasePoint((Canvas.GetLeft(relatedGrid), Canvas.GetTop(relatedGrid)),
                (relatedRect.Width, relatedRect.Height), type, true));
            Updates.UpdatePoints(points, type);

            return points;
        }

        public Grid MakeGrid()
        {
            var newGrid = new Grid();
            var rect = MakeRectangle();

            newGrid.Children.Add(rect);

            var stackPanel = MakeStackPanel(rect);
            newGrid.Children.Add(stackPanel);

            return newGrid;
        }

        private Rectangle MakeRectangle()
        {
            var rect = new Rectangle
            {
                Width =                 rectWidth,
                Height =                rectHeight,
                VerticalAlignment =     VerticalAlignment.Top,
                HorizontalAlignment =   HorizontalAlignment.Left,
                Fill =                  Brushes.AntiqueWhite,
                Stroke =                Brushes.Black,
                StrokeThickness =       0.5,
            };

            return rect;
        }

        private StackPanel MakeStackPanel(Rectangle rect)
        {
            var stackPanel = new StackPanel() { Margin = new Thickness(10, 10, 5, 10) };
            List<TextBlock> textBlocks = new List<TextBlock>() {
                new TextBlock { Name = "Name",
                    MaxHeight = rect.Height - stackPanel.Margin.Top - 40,
                    Width = rect.Width - stackPanel.Margin.Left - stackPanel.Margin.Right,
                    TextTrimming = TextTrimming.WordEllipsis,
                    TextWrapping = TextWrapping.Wrap},
                new TextBlock { Name = "BirthDate" },
                new TextBlock { Name = "DeathDate" }
            };

            foreach (var txtBlock in textBlocks)
            {
                stackPanel.Children.Add(txtBlock);
            }

            return stackPanel;
        }
    }
}
