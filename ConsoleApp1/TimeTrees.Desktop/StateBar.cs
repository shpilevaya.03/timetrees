﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace TimeTrees.Desktop
{
    internal enum StateBar
    {
        None,
        AddPerson,
        RemovePerson,
        Connection,
    }
}
