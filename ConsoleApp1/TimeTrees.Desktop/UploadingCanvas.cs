﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using TimeTrees.Core;

namespace TimeTrees.Desktop.Tools
{
    class UploadingCanvas
    {
        ToolArgs toolArgs;

        public UploadingCanvas(ToolArgs toolArgs)
        {
            this.toolArgs = toolArgs;
        }

        public void OpenCanvas()
        {
            OpenFileDialog openDlg = new OpenFileDialog();
            openDlg.Filter = "Текст формата json (*.json)|*.json";
            if (openDlg.ShowDialog() == true)
            {
                CanvasProperties.Path = openDlg.FileName;
                CanvasProperties.isUploaded = true;
                DrawCanvas(FileReader.ParseGridInfosJson(openDlg.FileName));
            }
        }

        void DrawCanvas(List<GridInfo> gridInfos)
        {
            DrawGrids(gridInfos);
            DrawConnections();
        }

        void DrawGrids(List<GridInfo> gridInfos)
        {
            toolArgs.canvas.Children.Clear();
            var settings = new SettingsShapes();
            foreach (var grid in gridInfos)
            {
                var newGrid = settings.MakeGrid();

                Canvas.SetLeft(newGrid, grid.coordinates.left);
                Canvas.SetTop(newGrid, grid.coordinates.top);

                var shape = new PersonShape() { Info = grid.person, GridShape = newGrid };
                toolArgs.personShapeRepo.AddPersonShape(shape);

                Updates.UpdateTextBoxes(newGrid, grid.person);

                toolArgs.canvas.Children.Add(newGrid);
            }
        }

        void DrawConnections()
        {
            var settings = new SettingsShapes();
            var grids = toolArgs.canvas.Children.OfType<Grid>().ToList();
            foreach (var grid in grids)
            {
                var rect = grid.Children.OfType<Rectangle>().FirstOrDefault();
                var personShape = toolArgs.personShapeRepo.FindPersonShape(grid);
                foreach (var relativeInfo in personShape.Info.RelativesIDs)
                {
                    var connection = new ConnectionInfo();
                    connection.Person = personShape;
                    connection.RelativeType = relativeInfo.type;
                    var relatedPerson = toolArgs.personShapeRepo.GetPersonShapes().Find(sh => sh.Info.ID == relativeInfo.ID);
                    connection.RelatedPerson = relatedPerson;
                    if (!ShapeInfoExtensions.ConnectionIsExists(toolArgs.personShapeRepo.GetConnectionInfos(), connection))
                    {
                        Polyline polyline = settings.DrawPolyline(connection.RelativeType);
                        var relatedGrid = grids.Find(gr => gr == relatedPerson.GridShape);
                        PointCollection points = settings.GetPoints(grid, relatedGrid, connection.RelativeType);
                        
                        polyline.Points = points;

                        connection.Connection = polyline;
                        toolArgs.personShapeRepo.AddConnection(connection);
                        toolArgs.canvas.Children.Add(polyline);
                    }
                }
            }
        }
    }
}
