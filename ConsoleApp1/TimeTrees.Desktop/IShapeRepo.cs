﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;
using TimeTrees.Core;

namespace TimeTrees.Desktop
{
    interface IShapeRepo
    {
        void RemoveShape(PersonShape personShape);
        void RemoveShape(Grid grid);
        void RemoveData();
        void AddPersonShape(PersonShape personShape);
        void AddConnection(PersonShape personShape, Polyline connection, PersonShape relatedPerson);
        void AddConnection(ConnectionInfo connection);
        void AddRectangleConnectionInfo(ConnectionInfo shapeInfo);
        void RemoveConnection(PersonShape personShape, PersonShape removeShape);
        PersonShape                     FindPersonShape(Grid grid);
        ConnectionInfo                  FindConnectionInfo(PersonShape person, PersonShape relatedPerson);
        List<ConnectionInfo>            GetConnectionInfos();
        List<PersonShape>               GetPersonShapes();
        List<Polyline>                  GetPolylines(Grid grid);
    }
}
