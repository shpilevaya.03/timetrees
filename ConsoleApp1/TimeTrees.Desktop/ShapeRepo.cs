﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;
using TimeTrees.Core;

namespace TimeTrees.Desktop
{
    internal class ShapeRepo : IShapeRepo
    {
        private List<ConnectionInfo>   connectionInfos = new List<ConnectionInfo>();
        private List<PersonShape>      personShapes = new List<PersonShape>();

        public void AddConnection(PersonShape personShape, Polyline connection, PersonShape relatedPerson)
        {
            var newPersonShape = new ConnectionInfo() 
            { 
                Person =        personShape,
                Connection =    connection,
                RelatedPerson = relatedPerson
            };

            connectionInfos.Add(newPersonShape);
        }

        public void AddConnection(ConnectionInfo connection)
        {
            connectionInfos.Add(connection);
        }

        public void AddPersonShape(PersonShape personShape)
        {
            personShapes.Add(personShape);
        }

        public void AddRectangleConnectionInfo(ConnectionInfo shapeInfo)
        {
            connectionInfos.Add(shapeInfo);
        }

        public PersonShape FindPersonShape(Grid grid)
        {
            foreach (var personShape in personShapes)
                if(personShape.GridShape == grid)
                    return personShape;
            return null;
        }

        public List<Polyline> GetPolylines(Grid grid)
        {
            List<Polyline> connections = new List<Polyline>();

            foreach (var shapeInfo in connectionInfos)
            {
                if(shapeInfo.Person.GridShape == grid || shapeInfo.RelatedPerson.GridShape == grid)
                    connections.Add(shapeInfo.Connection);
            }

            return connections;
        }

        public List<ConnectionInfo> GetConnectionInfos()
        {
            return connectionInfos.ToList();
        }

        public List<PersonShape> GetPersonShapes()
        {
            return personShapes.ToList();
        }

        public void RemoveData()
        {
            connectionInfos.Clear();
            personShapes.Clear();
        }

        public void RemoveConnection(PersonShape personShape, PersonShape removeShape)
        {
            var itemToRemove = connectionInfos.SingleOrDefault(
                shapeInfo => shapeInfo.Person == personShape && shapeInfo.RelatedPerson == removeShape
                );

            if (itemToRemove != null)
                connectionInfos.Remove(itemToRemove);
        }

        public void RemoveShape(PersonShape personShape)
        {
            List<ConnectionInfo> itemsToRemove = connectionInfos.FindAll(
                shapeInfo => shapeInfo.Person == personShape || shapeInfo.RelatedPerson == personShape
                );

            foreach (var shapeInfo in itemsToRemove)
            {
                if(connectionInfos.Contains(shapeInfo))
                {
                    connectionInfos.Remove(shapeInfo);
                }
            }
        }

        public void RemoveShape(Grid grid)
        {
            RemoveShape(FindPersonShape(grid));
        }

        public ConnectionInfo FindConnectionInfo(PersonShape person, PersonShape relatedPerson)
        {
            foreach (var connection in connectionInfos)
            {
                if(connection.Person == person && connection.RelatedPerson == relatedPerson ||
                    connection.RelatedPerson == person && connection.Person == relatedPerson)
                    return connection;
            }
            return null;
        }
    }
}
