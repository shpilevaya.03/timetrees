﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees.Desktop
{
    internal class ShapeInfoExtensions
    {
        public static bool ConnectionIsExists(List<ConnectionInfo> shapeInfos, ConnectionInfo shape)
        {
            foreach (ConnectionInfo shapeInfo in shapeInfos)
            {
                if((shapeInfo.Person == shape.Person && shapeInfo.RelatedPerson == shape.RelatedPerson) ||
                    (shapeInfo.RelatedPerson == shape.Person && shapeInfo.Person == shape.RelatedPerson))
                    return true;
            }
            return false;
        }
    }
}
