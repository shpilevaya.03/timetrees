﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimeTrees.Core
{
    public class PersonRepo : IPersonRepo
    {
        private static List<Person> _people;

        public void Init()
        {
            _people = FileReader.GetPeople();
        }

        public List<Person> GetAllPeople()
        {
            return _people.ToList();
        }

        public void AddPerson(Person person)
        {
            _people.Add(person);
        }

        public void RemovePerson(Person person)
        {
            _people.Remove(person);
        }
    }
}
