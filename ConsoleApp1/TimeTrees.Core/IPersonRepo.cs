﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees.Core
{
    public interface IPersonRepo
    {
        List<Person> GetAllPeople();
        void AddPerson(Person person);
        void RemovePerson(Person person);
    }
}
