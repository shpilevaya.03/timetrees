﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimeTrees.Core
{
    public class TimelineEventRepo : ITimelineEventRepo
    {
        private static List<TimelineEvent> _timelines;

        public void Init()
        {
            _timelines = FileReader.GetTimelineEvents();
        }

        public List<TimelineEvent> GetAllTimelineEvents()
        {
            return _timelines.ToList();
        }

        public void AddTimelineEvent(TimelineEvent timelineEvent)
        {
            _timelines.Add(timelineEvent);
        }

        public void RemoveTimelineEvent(TimelineEvent timelineEvent)
        {
            _timelines.Remove(timelineEvent);
        }
    }
}
