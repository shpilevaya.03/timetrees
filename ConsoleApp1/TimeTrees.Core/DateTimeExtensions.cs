﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace TimeTrees.Core
{
    public static class DateTimeExtensions
    {
        public static string DateTimeFormat = "yyyy-MM-dd";
        public static DateTime? ToDateTime(string data)
        {
            DateTime date = new DateTime();

            if (!DateTime.TryParseExact(data, "yyyy-MM-dd", CultureInfo.InvariantCulture,
                DateTimeStyles.None, out date))
                if (!DateTime.TryParseExact(data, "yyyy-MM", CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out date))
                    if (!DateTime.TryParseExact(data, "yyyy", CultureInfo.InvariantCulture,
                        DateTimeStyles.None, out date))
                        return null;

            return date;
        }

        public static string ToShortDateString(DateTime dateTime)
        {
            return dateTime.ToString(DateTimeFormat, CultureInfo.InvariantCulture);
        }
    }
}
