﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimeTrees.Core
{
    public class PeopleIDGenerator
    {
        public static int MaxID { get; set; }

        public static int MakeNewID(List<Person> people)
        {
            return people.Max(x => x.ID) + 1;
        }
    }
}
