﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeTrees.Core;

namespace TimeTrees.Core
{
    public class GridInfo
    {
        public (double left, double top) coordinates { get; set; }
        public Person person { get; set; }

        public GridInfo((double, double) coordinates, Person person)
        {
            this.coordinates = coordinates;
            this.person = person;
        }
    }
}
