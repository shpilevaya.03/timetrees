﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TimeTrees.Core
{
    public class FileWriter
    {
        private const string pathNamePeople = "people";
        private const string pathNameTimeline = "timeline";

        public FileWriter()
        {
            if (!File.Exists(pathNamePeople + ".csv") || !File.Exists(pathNamePeople + ".json")) File.WriteAllLines(pathNamePeople + ".csv", GeneratePeopleData());
            if (!File.Exists(pathNameTimeline + ".csv") || !File.Exists(pathNameTimeline + ".json")) File.WriteAllLines(pathNameTimeline + ".csv", GenerateTimelineData());
        }

        public void SaveTimelineEvents(List<TimelineEvent> timeline)
        {
            GenerateJson(pathNameTimeline + ".json", timeline);

            var output = "";
            foreach (var item in timeline)
            {
                var IDsString = "";
                foreach (var person in item.EventPeople)
                {
                    IDsString += person.ID + ",";
                }
                if (item.EventPeople.Count > 0)
                    IDsString = IDsString.Remove(IDsString.Length - 1);
                output += item.EventDate.ToString("yyyy-MM-dd") + ";" + item.EventDescription + ";" + IDsString + "\n";
            }

            File.WriteAllText(pathNameTimeline + ".csv", output);
        }

        public void SavePeople(List<Person> person)
        {
            GenerateJson(pathNamePeople + ".json", person);

            string output = "";
            foreach (var item in person)
            {
                string parentsIDs = "";
                foreach (var parent in item.RelativesIDs)
                {
                    parentsIDs += parent + ",";
                }
                if (item.RelativesIDs.Count > 0)
                    parentsIDs = parentsIDs.Remove(parentsIDs.Length - 1);

                output += item.ID + ";" + item.Name + ";" + item.BirthDate.ToString("yyyy-MM-dd") + ";" + (item.DeathDate == null ? parentsIDs : ((DateTime)item.DeathDate).ToString("yyyy-MM-dd") + ";" + parentsIDs) + "\n";
            }

            File.WriteAllText(pathNamePeople + ".csv", output);
        }

        public static void GenerateJson(string path, object obj)
        {
            string json = JsonConvert.SerializeObject(obj);
            File.WriteAllText(path, json);
        }

        static string[] GenerateTimelineData()
        {
            return new[] {
                "1941;Битва за Москву;2",
                "1944;Белорусская операция;3",
                "1942;Битва за Кавказ;5"
            };
        }

        static string[] GeneratePeopleData()
        {
            return new[]
            {
                "1;Иосиф Родионович Апанасенко;1890;1943",
                "2;Павел Артемьевич Артемьев;1897;1979",
                "7;Клементина Сергеевна Третьякова;1880;1980",
                "3;Иван Христофорович Баграмян;1897;1982",
                "4;Иван Александрович Богданов;1897;1942",
                "5;Семён Михайлович Будённый;1883;1973",
                "6;Климент Ефремович Ворошилов;1881;1969",
                "8;Елизавета Олеговна Соболева;1910;2010;2,7",
            };
        }
    }
}
