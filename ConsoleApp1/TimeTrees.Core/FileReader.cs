﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TimeTrees.Core
{
    public class FileReader
    {
        private const int idInd = 0;
        private const int nameInd = 1;
        private const int birthDateInd = 2;
        private const int deathDateInd = 3;
        private const int parentsIDsInd = 4;

        private const int timelineDateInd = 0;
        private const int timelineDescriptionInd = 1;
        private const int timelinePeopleIDs = 2;

        private const string pathNamePeople = "people";
        private const string pathNameTimeline = "timeline";

        public static List<Person> GetPeople()
        {
            if (File.Exists(pathNamePeople + ".json"))
            {
                return ParsePersonJson(pathNamePeople + ".json");
            }
            else if (File.Exists(pathNamePeople + ".csv"))
            {
                return ParsePersonCsv(pathNamePeople + ".csv");
            }
            else
                throw new Exception("Файл не существует, либо не имеет расширения json или csv");
        }

        public static List<TimelineEvent> GetTimelineEvents()
        {
            if (File.Exists(pathNameTimeline + ".json"))
            {
                return ParseTimelineJson(pathNameTimeline + ".json");
            }
            else if (File.Exists(pathNameTimeline + ".csv"))
            {
                return ParseTimelineCsv(pathNameTimeline + ".csv");
            }
            else
                throw new Exception("Файл не существует, либо не имеет расширения json или csv");
        }

        static List<Person> ParsePersonCsv(string path)
        {
            string[][] data = ParseText(path);
            List<Person> dataSplit = new List<Person>();

            //for (int i = 0; i < data.Length; i++)
            //{
            //    string[] elems = data[i];

            //    Person person = new Person(
            //        int.Parse(elems[idInd]),
            //        elems[nameInd],
            //        DateTimeExtensions.ToDateTime(elems[birthDateInd]),
            //        elems[deathDateInd] != "" && DateTimeExtensions.ToDateTime(elems[deathDateInd]) != new DateTime()
            //            ? DateTimeExtensions.ToDateTime(elems[deathDateInd])
            //            : (DateTime?)null,
            //        elems[deathDateInd] == null || elems.Length == 5 ? Array.ConvertAll(elems[elems.Length == 5 ? parentsIDsInd : parentsIDsInd - 1].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries),
            //        num => int.Parse(num)).ToList() : new List<(int, RelativeType)>()
            //        );

            //    dataSplit.Add(person);
            //}

            return dataSplit;
        }

        static List<Person> ParsePersonJson(string path)
        {
            string json = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<List<Person>>(json);
        }

        static List<TimelineEvent> ParseTimelineCsv(string path)
        {
            PersonRepo personRepo = new PersonRepo();

            string[][] data = ParseText(path);
            List<TimelineEvent> dataSplit = new List<TimelineEvent>();
            for (int i = 0; i < data.Length; i++)
            {
                string[] elems = data[i];

                TimelineEvent timeline = new TimelineEvent(
                    (DateTime)DateTimeExtensions.ToDateTime(elems[timelineDateInd]),
                    elems[timelineDescriptionInd],
                    Array.ConvertAll(elems[timelinePeopleIDs].Split(new[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries),
                    num => personRepo.GetAllPeople().First(x => x.ID == int.Parse(num))).ToList()
                    );

                dataSplit.Add(timeline);
            }

            return dataSplit;
        }

        static List<TimelineEvent> ParseTimelineJson(string path)
        {
            string json = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<List<TimelineEvent>>(json);
        }

        public static List<GridInfo> ParseGridInfosJson(string path)
        {
            string json = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<List<GridInfo>>(json);
        }

        static string[][] ParseText(string path)
        {
            return Array.ConvertAll(File.ReadAllText(path, Encoding.Default).Split(new[] { '\n', '\r' },
                StringSplitOptions.RemoveEmptyEntries), arr => arr.Split(';'));
        }
    }
}
