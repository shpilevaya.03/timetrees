﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TimeTrees.Core
{
    /// <summary>
    /// Представляет информацию о человеке
    /// </summary>
    public class Person
    {
        public int ID { get; private set; }
        public string Name { get; private set; }
        public DateTime BirthDate { get; private set; }
        public DateTime? DeathDate { get; private set; }
        public List<(int ID, RelativeType type)> RelativesIDs { get; private set; }

        public Person(string name, DateTime? birthDate, DateTime? deathDate, List<(int, RelativeType)> relatives) 
        {
            PeopleIDGenerator.MaxID++;
            ID = PeopleIDGenerator.MaxID;
            Name =          name;
            BirthDate =     birthDate is null ? new DateTime() : CheckBirthDate((DateTime)birthDate);
            DeathDate =     CheckDeathDate(BirthDate, deathDate);
            RelativesIDs =  relatives == null ? new List<(int, RelativeType)>() : relatives;
        }

        [JsonConstructor]
        public Person(int id, string name, DateTime? birthDate, DateTime? deathDate, List<(int, RelativeType)> relatives)
        {
            if (id > PeopleIDGenerator.MaxID)
            {
                PeopleIDGenerator.MaxID = id;
            }

            ID =            id;
            Name =          name;
            BirthDate =     birthDate is null ? new DateTime() : CheckBirthDate((DateTime)birthDate);
            DeathDate =     CheckDeathDate(BirthDate, deathDate);
            RelativesIDs =  relatives == null ? new List<(int, RelativeType)>() : relatives;
        }

        public DateTime CheckBirthDate(DateTime dateTime)
        {
            if(dateTime > DateTime.Today)
                return BirthDate;
            return dateTime;
        }

        public DateTime? CheckDeathDate(DateTime birthDate, DateTime? deathDate)
        {
            if (deathDate > birthDate)
                return deathDate;
            else return DeathDate;
        }

        public int FindAge()
        {
            return (int)((DateTime.Today - BirthDate).TotalDays / 365.242199);
        }

        public void AddRelative(Person person, RelativeType type)
        {
            RelativesIDs.Add((person.ID, type));
        }

        public void RemoveRelative(Person person, RelativeType type)
        {
            RelativesIDs.Remove((person.ID, type));
        }

        public void RemoveRelatives()
        {
            RelativesIDs = null;
        }

        public void ChangeData(string name, DateTime? birthDate, DateTime? deathDate)
        {
            ChangeName(name);
            ChangeBirthDate(birthDate is null ? new DateTime() : (DateTime)birthDate);
            ChangeDeathDate(deathDate);
        }

        public void ChangeName(string name)
        {
            Name = name;
        }

        public void ChangeBirthDate(DateTime dateTime)
        {
            BirthDate = CheckBirthDate(dateTime);
        }

        public void ChangeDeathDate(DateTime? deathDate)
        {
            DeathDate = CheckDeathDate(BirthDate, deathDate);
        }
    }
}
