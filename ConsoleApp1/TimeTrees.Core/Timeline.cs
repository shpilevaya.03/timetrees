﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace TimeTrees.Core
{
	/// <summary>
	/// Представляет информацию о событии
	/// </summary>
	public class TimelineEvent
	{
		public DateTime EventDate { get; }
		public string EventDescription { get; }
		public List<Person> EventPeople { get; }

		public TimelineEvent(DateTime eventDate, string eventDescription, List<Person> eventPeople)
        {
			EventDate = eventDate;
			EventDescription = eventDescription;
			EventPeople = eventPeople;
        }
	}

	public class Timeline
	{
		/// <summary>
		/// Возвращает максимальную разницу во времени между событиями
		/// </summary>
		/// <param name="timelines">Массив массивов, имеющих структуру {дата_события};{описание_события}</param>
		/// <returns></returns>
		public static (int years, int months, int days) MaximumDifference(List<TimelineEvent> timelines)
		{
			(DateTime minDate, DateTime maxDate) = GetMinAndMaxDate(timelines);

            (int years, int months, int days) result = GetMaxDiffDates(minDate, maxDate);
			return result;
		}

		static (DateTime, DateTime) GetMinAndMaxDate(List<TimelineEvent> timelines)
		{
			DateTime minDate = DateTime.MaxValue, maxDate = DateTime.MinValue;

			foreach (TimelineEvent timeline in timelines)
			{
                DateTime date = timeline.EventDate;

				if (minDate > date)
					minDate = date;
				if (maxDate < date)
					maxDate = date;
			}

			return (minDate, maxDate);
        }

		static (int years, int months, int days) GetMaxDiffDates(DateTime d1, DateTime d2)
		{
			return (d2.Year - d1.Year, d2.Month - d1.Month, d2.Day - d1.Day);
		}
	}
}
