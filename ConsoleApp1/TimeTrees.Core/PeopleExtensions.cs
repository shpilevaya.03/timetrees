﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees.Core
{
    public class PeopleExtensions
    {
        /// <summary>
        /// Возвращает имена всех людей, которые родились в високосный год и их возраст не более 20 лет.
        /// </summary>
        /// <param name="person">Массив массивов, имеющих структуру {id};{Имя};{дата_рождения};{дата_смерти};{id родителей}</param>
        /// <returns></returns>
        public static List<Person> FindPeopleBornInLeapYear(List<Person> person)
        {
            List<Person> people = new List<Person>();

            for (int i = 0; i < person.Count; i++)
            {
                if (person[i].DeathDate == null)
                {
                    int age = person[i].FindAge();
                    if (age <= 20 && DateTime.IsLeapYear(person[i].BirthDate.Year))
                        people.Add(person[i]);
                }
            }
            return people;
        }
    }
}
