﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees.Core
{
    public interface ITimelineEventRepo
    {
        List<TimelineEvent> GetAllTimelineEvents();
        void AddTimelineEvent(TimelineEvent timelineEvent);
        void RemoveTimelineEvent(TimelineEvent timelineEvent);
    }
}
