﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees.Core
{
    public enum RelativeType
    {
        [Description("Неизвестная связь")]
        None,
        [Description("Родитель")]
        Parent,
        Child,
        [Description("Братья/сёстры/брат и сестра")]
        SisterOrBrother,
        [Description("Супруги")]
        Spouses,
        [Description("Бывшие супруги")]
        FormerSpouses,
    }
}
